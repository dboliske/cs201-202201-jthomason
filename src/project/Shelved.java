package project;
 
public class Shelved extends Product { // this class doesn't have 
	// any new variables so all the methods are created using the methods
	// of the super class
	
	public Shelved() {
		super();
	}
	
	public Shelved(String name, double price) {
		super(name,price);
	}
	
	public String toString(){
		return super.toString();
	}
	
	public boolean equals(Object obj) {
		return super.equals(obj);
	}
	
	@Override
	public String toCSV() {
		return "Shelved," + super.csvData();
	}
	
	
}

