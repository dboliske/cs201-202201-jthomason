package project;

public class Product { // creating variables
	private String name;
	private double price;
	
	public Product() { // creating default constructor
		this.name = "Apple";
		this.price = 1.00;
	}
	
	public Product(String name, double price) { // creating non-default constructor
		this.name = name;
		this.price = 1.00;
		setPrice(price);
	}
	
	public void setName(String name) { // creating mutator methods
		this.name = name;
	}
	
	public void setPrice(double price) {
		this.price = price;
	}
	
	public String getName() { // creating accessor methods
		return name; 
	}
	
	public double getPrice() {
		return price;
	}

	@Override
	public boolean equals(Object obj) { //creating equals method
		if(!(obj instanceof Product)) {
			return false; // comparing objects
		}
		
		Product p = (Product)obj; 
		if(!this.name.equals(p.getName())) { // comparing name and price
			return false;
		} else if(this.price != p.getPrice()) {
			return false;
		}
		return true;
	}
	
	@Override
	public String toString() { // creating toString
		return "Product: " + name + ", Price: " + price;
	}
	
	protected String csvData() { 
		return name + "," + price;
	}
	
	public String toCSV() { // using csv data for toCSV method
		return "Product," + csvData(); 
	}
	
	
	
}
