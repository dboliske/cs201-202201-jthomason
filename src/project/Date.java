package project;

public class Date {
	private int day; // creating variables
	private int month;
	private int year;

	public Date() { // creating default constructor
		day = 20;
		month = 5;
		year = 2003;
	}
	
	public Date(int day, int month, int year) { // creating non default constructor
		this.day = day;
		setDay(day);
		this.month = month;
		setMonth(month);
		this.year = year;
		setYear(year);
	}
	
	public void setDay(int day) { // creating mutator methods
		if(day >= 1 && day <= 31) { // making sure the day entered is valid
			this.day = day;
		}
	}
	
	public void setMonth(int month) {
		if(month >= 1 && month <= 12) { // making sure the month entered is valid
			this.month = month;
		}
	}
	
	public void setYear(int year) { 
		this.year = year;
	}
	
	public int getDay() { // creating accessor methods
		return day;
	}
	
	public int getMonth() {
		return month;
	}
	
	public int getYear() {
		return year;
	}
	
	public String toString() { // creating toString method
		return year + "-" + 
			(month<10?("0" + month): month) + "-" + 
			(day<10?("0" + day): day);
	}
	
	public boolean isLeap() { // creating method to check if the year entered is a leap year 
		// to use in the validDate method
		int year = this.year;
		if(year % 4 == 0) {
			if(year % 100 == 0) {
				if(year % 400 == 0)
					return true;
				else {
					return false;
				}
			} else {
				return true;
			}
		} else {
			return false;
		}
	}
	
	public boolean validDate() { // creating valid date method
		
		int m = this.month;
		int d = this.day;
		Date dt = new Date(this.day,this.month,this.year); // creating new date object to call isLeap method
		boolean leap = dt.isLeap(); // calling isLeap method
		
		

		if(m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) { // checking for valid day
			if(d >= 1 && d <= 31) {
				return true;
			}
		}
			
		if (m == 4 || m == 6 || m == 9 || m == 11) {
			if(d >= 1 && d <= 30) {
				return true;
			}
		}
			
		if (m == 2) { // if the month is February, then the conditions are different depending on whether or not the year is a leap 
			// year
			if(leap) {
				if(d >= 1 && d <= 29) {
					return true;
				}
			} else {
				if(d >= 1 && d <= 28) {
					return true;
				}
			}
		}
		
		return false;

	}
	

	
	public boolean before(Date d) { // writing before method
		if(this.year > d.getYear()) { // comparing years 
			return false;
		} else if(this.year == d.getYear()) { // comparing months
			if (this.month > d.getMonth()) {
				return false;
			} else if(this.month == d.getMonth()) {
				if(this.day > d.getDay()) { // comparing days
					return false;
				}
			}
		}		
		return true;
	}
	
	public boolean equals(Object obj) { // creating equals method
		if(!(obj instanceof Date)) { // if object is not an instance of date, return false
			return false;
		}
		
		Date dt = (Date)obj; // typecasting object
		if(this.year != dt.getYear()) { // comparing year
			return false;
		} else if(this.month != dt.getMonth()) { // comparing month
			return false;
		} else if(this.day != dt.getDay()) { // comparing day
			return false;
		} 
		return true;
	}
	
	
}
