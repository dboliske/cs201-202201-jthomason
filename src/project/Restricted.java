package project;

public class Restricted extends Product { 
	
	private int age; // creating variables
	
	public Restricted() { // using default super constructor to create default constructor of restricted
		super();
		age = 0;
	}
	
	public Restricted(String name, double price, int age) { // using non default 
		// super constructor to create non default constructor of restricted
		super(name,price);
		age = 21;
		setAge(age);
	}
	
	public void setAge(int age) { // creating mutator method
		this.age = age;
	}
	
	public int getAge() { // creating accessor method
		return age;
	}
	
	public boolean isOfAge(int age) { // checking if user is of age to purchase
		if(this.age > age) {
			return false;
		}
		else {
			return true;
		}
	}
	
	@Override
	public boolean equals(Object obj) { // overriding equals method of super class 
		if(!super.equals(obj)) {
			return false;
		} else if(!(obj instanceof Restricted)) {
			return false;
		} 
		
		Restricted r = (Restricted)obj; // comparing age restrictions
		if(this.age != r.getAge()) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() { // overriding toString of super class
		return super.toString() + ", Age: " + age + "+";
	}
	
	@Override
	public String toCSV() { // overriding toCSV of super class
		return "Restricted," + super.csvData() + "," + age;
	}

}
