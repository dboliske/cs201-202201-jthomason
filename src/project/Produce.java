package project;

public class Produce extends Product {
	
	private Date expiration; // creating expiration variable of type date
	
	public Produce() {
		super(); // using super constructor and default date constructor to create default constructor of produce
		expiration = new Date();
	}
	
	public Produce(String name, double price, Date date) { // creating non default constructor, using
		// non default constructors of the super class and date class
		super(name,price);
		int d = date.getDay();
		int m = date.getMonth();
		int y = date.getYear();
		expiration = new Date(d,m,y);
	}
	
	public void setExpiration(Date d) { // creating mutator method
		if(d.validDate()) {
			expiration = new Date(d.getDay(),d.getMonth(),d.getYear());
		}
	}
	
	public Date getExpiration() { // creating accessor method
		return expiration;
	}
	
	@Override
	public boolean equals(Object obj) { // overriding super class equals method
		if(!(super.equals(obj))) { // using super class equals method
			return false;
		} else if (!(obj instanceof Produce)) {
			return false;
		}
		
		Produce p = (Produce)obj; // comparing expiration dates
		if(!(this.expiration.equals(p.getExpiration()))) {
			return false;
		}
		
		return true;
	}
	
	@Override
	public String toString() { // overriding super toString method and using super toString
		return super.toString() + " Expiration Date: " + expiration.toString();
	}
	
	@Override
	public String toCSV() { // overriding super toCSV method and using super toCSV
		return "Produce," + super.csvData() + "," + expiration;
	}
	
	public boolean isExpired(Date d) { // using before method of date class to see if the product
		// is expired
		if(expiration.before(d)) {
			return true;
		} 
		return false;
	}
	
	

	
	
	
	

}
