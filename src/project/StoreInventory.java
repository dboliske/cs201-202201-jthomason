package project;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;

public class StoreInventory {

	public static ArrayList<Product> readFile(String filename) { // creating file reader method
		ArrayList<Product> inventory = new ArrayList<Product>(); // creating product array list
		 
		try { // using try catch in order to catch errors in reading in the file
			File f = new File(filename); // creating file object 
			Scanner input = new Scanner(f); // creating scanner
			
			while(input.hasNextLine()) { // searching csv file
				try {
					String line = input.nextLine(); // splitting each line by commas
					String [] values = line.split(",");
					Product p = null; // initializing product and date
					Date dt = null; 
					switch(values[0].toLowerCase()) { // using to lower case to prevent errors
					case "produce": // in the case of produce, the date object must also be created, since the csv file 
						// has dates of the format DD/MM/YY, the information is split by the character "/"
						String filedate = values[3];
						String [] dateinfo = filedate.split("/");
						int d = Integer.parseInt(dateinfo[0]);
						int m = Integer.parseInt(dateinfo[1]);
						int y = Integer.parseInt(dateinfo[2]);
						dt = new Date(d,m,y); // creating new date object
						p = new Produce(values[1],Double.parseDouble(values[2]),dt); // creating produce object
						break;
					case "restricted":
						p = new Restricted(values[1],Double.parseDouble(values[2]),Integer.parseInt(values[3])); // creating restricted object
						break;
					case "shelved": 
						p = new Shelved(values[1],Double.parseDouble(values[2])); // creating shelved object
						break;
					} inventory.add(p); // adding product to arraylist
 				} catch(Exception e) { // catches error in the file itself
					System.out.println("File Error");
				}
			}
			
			input.close(); // closing scanner
		} catch(FileNotFoundException fnf){ // displaying file not found message
			System.out.println("File Not Found");
		} catch(Exception e) { // displaying generic error message
			System.out.println("Error occurred reading in file");
		}
		return inventory; // returning array list
	}
	
	public static ArrayList<Product> menu(Scanner input, ArrayList<Product> inventory, Date dt, int age){ // creating menu method
		boolean done = false; // initializing variables
		String userSearch = null;
		do { // printing menu
			System.out.println("1. Add Item to Inventory");
			System.out.println("2. Edit Cart");
			System.out.println("3. Modify Item in Inventory");
			System.out.println("4. Search for Item");
			System.out.println("5. Display Current Inventory");
			System.out.println("6. Exit");
			System.out.println("Choice: ");
			int userChoice = Integer.parseInt(input.nextLine()); // reading in user input
			switch(userChoice) { // switch case that calls on method depending on user's choice
			case 1:
				inventory = addItem(inventory,input,dt);	// calling addItem method
				break;
			case 2:
				inventory = removeItem(inventory,input,age); // calling removeItem method
				break;
			case 3:
				inventory = modifyItem(inventory,input); // calling modifyItem method
				break;
			case 4: 
				System.out.println("Enter the name of the product you want to search for: "); // prompting user for name 
				// of product they want to search for
				try { // using try catch in case user enters invalid input
					 userSearch = input.nextLine(); // reading in user input
				} catch(Exception e) {
					System.out.println("Invalid product name, returning to menu"); // displaying error message
				}
					int index = search(inventory,userSearch); // calling search method
					if(index == -1) { // seeing if item is in inventory
						System.out.println("Item Not Found, returning to menu"); // displaying message if item is not found
					} else {
						System.out.println("Item in Stock, Item Number: " + (index + 1)); // displaying message if item is found
						System.out.println(inventory.get(index)); // displaying item information
					}
					break;
			case 5:
				display(inventory,input); // calling display method
				break;
			case 6:
				done = true; // exiting out of menu by changing boolean value of control variable
				break;
			default: 
				System.out.println("Please enter a valid option: "); // in case user enters an invalid input
			}
		}  while(!done); // while loop to make sure menu runs until user chooses exit
		 
		return inventory; // return arraylist
			 
		}

	
	public static Date enterDate(Scanner input, String prompt) { // creating method to read in dates and create date objects
		Date defaultdt = new Date(); // creating date using default constructor in case user enters invalid date
		System.out.println(prompt); // displays prompt
		String userdate = input.nextLine(); // reading in user input
		String[] expirationinfo = userdate.split("/"); // splitting user input by "/" character
		int d = 0; // initializing variables
		int m = 0;
		int y = 0;
		try { // using try catch in case user uses an invalid format
		 d = Integer.parseInt(expirationinfo[0]);  // assigning variables to user input
		 m = Integer.parseInt(expirationinfo[1]);
		 y = Integer.parseInt(expirationinfo[2]);
	} catch (Exception e) {
		System.out.println("Error entering date, returning to menu"); // displaying error message
		return defaultdt;
	}
		Date userdt = new Date(d,m,y); // creating new date object

		
		if(userdt.validDate()) { // checking if the user entered date is valid
			defaultdt = userdt; // if it is valid, the method will return the user entered date
		}
		
		return defaultdt; // if the user enters an invalid date, the date of the default constructor will be used
	}
	 
	public static int enterAge(Scanner input, String prompt) { // creating method for user to enter age
		System.out.println(prompt); // prompting user
		int userAge = 0; // initializing age
		try { // using try catch in case user enters invalid input
			userAge = Integer.parseInt(input.nextLine()); // assigning userAge
		} catch(Exception e) {
			System.out.println("That is an invalid age, returning to menu"); // displaying error message
		}
		return userAge; // returning age
	}
	

	public static ArrayList<Product> addItem(ArrayList<Product> inventory, Scanner input, Date dt){ // creating method to add item
		// the method takes in a date object, at the beginning of the program the user is asked to enter today's date,
		// the addItem method checks if the user is attempting to add an expired item to the inventory according to today's date
		Product p = null; // initializing product object
		System.out.println("Enter the name of the product: "); // prompting user
		String name = input.nextLine(); // storing user response
		System.out.println("Enter the price of the product: ");
		double price = 0; // initializing variables
		int userAge = 0;
		try { // using try catch to prevent any invalid user input
			price = Double.parseDouble(input.nextLine());
		} catch(Exception e) {
			System.out.println("Invalid price; Returning to menu"); // displaying error message
			return inventory;} // return user to menu
		System.out.println("What type of product would you like to add?"); // displaying menu
		System.out.println("1. Produce");
		System.out.println("2. Restricted Item");
		System.out.println("3. Shelved Item");
		System.out.println("Choice: ");
		int userChoice = Integer.parseInt(input.nextLine()); // reading in user choice
		switch(userChoice) {
			case 1: 
				Date expodt = enterDate(input,"Please enter expiration date (Format DD/MM/YY): "); // prompting user for expiration date
				// using enterDate method
				if(expodt.before(dt)) { // checking if expiration date is before today's date
					System.out.println("Cannot add expired item to inventory, returning to menu"); // if it is, user is returned
					// to menu
					return inventory;
				}
				p = new Produce(name,price,expodt); // creating new produce object
				break;
			case 2:
				userAge = enterAge(input, "Please enter required age of purchase: "); // using enterAge method to 
				// obtain age restriction for restricted item
				p = new Restricted(name,price,userAge); // creating new restricted item
				break;
			case 3:
				p = new Shelved(name,price); // creating new shelved item
				break;
			default: 
				System.out.println("Invalid product type, returning to menu"); // returns user to menu if they enter invalid input
		}
		inventory.add(p); // adds product to inventory
		System.out.println("Item has been successfully added"); // display message
		return inventory; // return inventory
		}
	
	public static ArrayList<Product> removeItem(ArrayList<Product> inventory, Scanner input, int age) { // creating removeItem method
		ArrayList<Product> toRemove = new ArrayList<Product>(); // creating new arrayList that stores 
		int userChoice = 0; // initializing variables
		String userSearch = null;
		int index = 0;
		boolean done = false;
		Product p = null;
		Restricted userRest = null;
		double total = 0;
		do {
			System.out.println("1. Add Item to Cart"); // displaying menu
			System.out.println("2. Display Cart");
			System.out.println("3. Checkout");
			System.out.println("Choice: ");
			try { // try catch to ensure user inputs valid choice
				userChoice = Integer.parseInt(input.nextLine()); // reading in user input
			} catch(Exception e){
				System.out.println("Invalid response, returning to menu"); // displaying error message
				return inventory; // returning user to menu
			}
			switch(userChoice) {
			case 1: 
				System.out.println("Enter the name of the item you would like to add to your cart: "); // prompting user
				try {
					userSearch = input.nextLine(); // using try catch to ensure user enters valid input
				} catch (Exception e) {
					System.out.println("Invalid search term, returning to menu");
					return inventory;
				}
				
				index = search(inventory,userSearch); // storing index returned from search method
				if(index == -1) { // if item is not found, message is displayed and user is returned to the menu
					System.out.println("Item not found, returning to menu");
					return inventory;
				}
				
				p = inventory.get(index); // creating a new product based on the item user searched for
				
				if(p instanceof Restricted) { // if the product is a restricted item, then the method must check if the 
					// user is of valid age to add this item to their cart
					userRest = (Restricted)inventory.get(index); // typecasting the product the user searched for in order to use
					// isOfAge method to decide whether or not the user is able to add the item to their cart
					if(!(userRest.isOfAge(age))) { // check if user is off age
						System.out.println("You are not of valid age to purchase this product, returning to menu"); // returning user to menu
						return inventory;
					}
				}
				
				System.out.println("Item successfully added to cart"); // let the user know the item was successfully added
			
				toRemove.add(p); // add items requested by user to list that tracks what items they want to remove
				break;
			case 2: 
				for(Product prod : toRemove) { // indexes through list
					System.out.println(prod); // displays item
					total = total + prod.getPrice(); // keeps a running total
				} 
				System.out.println("Total: " + "$" + total); // displays total
				break;
			case 3:
				for(Product prod : toRemove) { // removes the items from the inventory
					inventory.remove(prod);
				}
				done = true; // exits out of menu
				break;
			default:
				System.out.println("Please enter a valid choice"); 
			}
		} while(!done);
		
		return inventory;
	}
	
	public static int search(ArrayList<Product> inventory, String name) { // creating search method
		for(Product p : inventory) { // indexes through list
			if(p.getName().equalsIgnoreCase(name)) { // using a sequential search and comparing each element to
				// the user entered info
				return inventory.indexOf(p); // uses indexOf method to return index of the product
				// the user searched for
			}
		}
		return -1; // returns -1 if the item is not found in the list
	}
	
	public static ArrayList<Product> modifyItem(ArrayList<Product> inventory, Scanner input){ // creating modify method
		Product userProd = null; // initializing variables
		int userChoice = 0;
		String userName = null;
		double userPrice = 0;
		System.out.println("What item would you like to modify?: "); // prompting user and saving their input
		String userSearch = input.nextLine(); 
		int index = search(inventory,userSearch); // calling search method to find the index of the item they requested
		if(index == -1) { // in case the item is not in the list
			System.out.println("Item not found, returning to menu");
			return inventory;
		} else { // creating new product object that matches what the user searched for
			userProd = inventory.get(index);
		}
		
		
		System.out.println("What would you like to modify?: ");
		System.out.println("1. Item Name");
		System.out.println("2. Item Price");
		if(userProd instanceof Produce) { // displays a different third option depending on whether the product is 
			// produce or a restricted item
			System.out.println("3. Expiration Date");
		}
		if(userProd instanceof Restricted) {
			System.out.println("3. Age Restriction");
		}
		try { // try catch to prevent invalid user input
			userChoice = Integer.parseInt(input.nextLine());
		} catch (Exception e) {
			System.out.println("Invalid choice, returning to menu");
			return inventory;
		}
		
		switch(userChoice) { 
			case 1: 
				System.out.println("Enter new name: "); // prompting user and storing their input
				try { // try catch in order to prevent invalid user input
					userName = input.nextLine();
				} catch(Exception e) {
					System.out.println("Invalid name entered, returning to menu");
					return inventory;
				}
				for(Product p : inventory) {  // indexing through inventory and comparing each item to the 
					// item the user requested
					if(p.equals(userProd)) {
						p.setName(userName); // if the items are equal, the change is applied to the item
					}
				}
				System.out.println(userProd.getName() + " has been changed to " + userName + ", returning to menu"); //
				break;
			case 2: 
				System.out.println("Enter new price: "); 
				try {
					userPrice = Double.parseDouble(input.nextLine());
				} catch(Exception e) {
					System.out.println("Invalid price entered, returning to menu");
					return inventory;
				}
				for(Product p : inventory) {
					if(p.equals(userProd)) {
						p.setPrice(userPrice);
					}
				}
				System.out.println("Price of " + userProd.getName() + " has been changed to " + userPrice + ", returning to menu");
				break;
			case 3:
				if(userProd instanceof Produce) { // case 3 depends on what type of item the product is
					
					Date userExpiration = enterDate(input,"Enter the new expiration date: "); // using enterDate method
					// for user to enter new expiration date
					Produce	userProduce = new Produce(userProd.getName(),userProd.getPrice(),userExpiration); // creating temporary 
					// produce object
				for(Product p : inventory) { // indexing through inventory
					if(p.equals(userProd)) { // comparing product to user entered product
						p = userProduce; // setting product equal to the modified object
					}
				}
				
				System.out.println("Expiration date of " + userProd.getName() + " has been changed to " + userExpiration);
				
				}
				if(userProd instanceof Restricted) { 
					int userAge = enterAge(input,"Enter the new age restriction: "); // using enterAge method
					// for user to enter age restriction
					Restricted userRestricted = new Restricted(userProd.getName(),userProd.getPrice(),userAge); // creating
					// new restricted object based on user modifications
					for(Product p : inventory) { // indexing through inventory
						if(p.equals(userProd)) { // comparing product to user entered product
							p = userRestricted; // setting product equal to modified object
						}
					}
				
				System.out.println("Age restriction of" + userProd.getName() + " has been changed to " + userAge + "+");
					
				}
				break;
		}
		return inventory;
	}
			
	
	public static void display(ArrayList<Product> inventory, Scanner input) { // creating display method
		System.out.println("Display Products: "); // display menu
		System.out.println("1. Display Produce: ");
		System.out.println("2. Display Restricted Items");
		System.out.println("3. Display Shelved Items");
		System.out.println("4. Display All Items");
		System.out.println("Choice: ");
		int userChoice = Integer.parseInt(input.nextLine()); // saving user input
		
		switch(userChoice) {
			case 1: 
				for(Product p: inventory) {
					if(p instanceof Produce) { // indexing through inventory and displaying all produce
						System.out.println(p);
					}
				}
				break;
			case 2:
				for(Product p: inventory) {
					if(p instanceof Restricted) { // indexing through inventory and displaying all restricted items
						System.out.println(p);
					}
				}
				break;
			case 3:
				for(Product p: inventory) {
					if(p instanceof Shelved) { // indexing through inventory and displaying all shelved items
						System.out.println(p);
					}
				}
				break;
			case 4:
				for(Product p: inventory) { // indexing through inventory and displaying all products
					System.out.println(p);
				}
				break;
			default:
				System.out.println("Invalid choice, returning to menu"); // returning user to menu if they enter invalid input
		}
	}
	
	public static void saveFile(String filename, ArrayList<Product> inventory) { // creating save file method
		try { // using try catch to prevent errors writing the file
			FileWriter writer = new FileWriter(filename); // creating file write object
			
			for (Product p : inventory) { // indexing through inventory
				writer.write(p.toCSV() + "\n"); // calling on toCSV method to write a formatted string to the file
				writer.flush(); // flushing file writer
			}
			
			writer.close(); // closing file writer
		} catch (Exception e) {
			System.out.println("Error saving to file."); // displaying error message
		}
	}
	
	
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // creating scanner
		System.out.println("Load file: ");
		String filename = "src/project/" + input.nextLine() + ".csv"; // prompting user for filename
		ArrayList<Product> inventory = readFile(filename); // calling read file method
		Date today = enterDate(input,"Enter today's date(Format DD/MM/YYYY): "); // prompting user for date to be used in 
		// other methods
		int userAge = enterAge(input, "Enter your age: "); // prompting user for their age to 
		// be used in other methods
		inventory = menu(input,inventory,today,userAge); // calling menu method
		System.out.println("Save file: ");
		String savefile = "src/project/" + input.nextLine() + ".txt";
		saveFile(savefile,inventory); // calling savefile method
		input.close(); // closing scanner
		System.out.println("Goodbye!"); 
	}


}
