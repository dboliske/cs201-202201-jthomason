// Jean-Yves Thomason; CS 201; Section One; 02/09/22; Exercise Three; ArrayMinimum
package labs.lab3;

public class ArrayMinimum {

	public static void main(String[] args) {
		int[] numbers = {72, 101, 108, 108, 111, 32, 101, 118, 101, 114, 121, 111, 110,
				101, 33, 32, 76, 111, 111, 107, 32, 97, 116, 32, 116, 104, 101, 115,
				101, 32, 99, 111, 111, 108, 32, 115, 121, 109, 98, 111, 108, 115, 58,
				32, 63264, 32, 945, 32, 8747, 32, 8899, 32, 62421}; // declaring array 
		double min = numbers[0]; // declaring minimum value at first value of array
		for(int i = 1; i < numbers.length; i++) { // looping through values of array excluding first value
			if(numbers[i] < min) { // evaluating whether one number is less than 
				min = numbers[i]; // setting minimum value to new minimum value
			}
		}
		System.out.println("The minimum value is " + min); // displaying minimum value 
	}
}
