// Jean-Yves Thomason; CS 201; Section One; 02/08/22; Exercise Two; userNumber2
package labs.lab3;

import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class userNumber2 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in); // creating scanner
		double[] userNums = new double[1]; // initializing array to store user values
		int count = 0; // initializing index variable
		boolean done = false; // creating flag variable
		while (!done) {
			System.out.println("Enter a number (enter done to stop): "); // prompting user for number
			String userInput = input.nextLine(); // saving user input
			if (userInput.equalsIgnoreCase("done")) { // evaluating user input
				done = true; // changing value of flag variable in case user enters "done"
			}
			else {
			userNums[count] = Double.parseDouble(userInput); // converting user input to double and 
			// saving in array
			count++; // incrementing count
			if (count == userNums.length) { // evaluating whether the array is full
				double[] bigger = new double[userNums.length + 1]; // creating temporary array,
				// incrementing by one to ensure that the numbers printing are only the numbers entered
				// by the user 
				for (int i = 0; i < userNums.length; i++) { // copying values of old array into larger
					// array
					bigger[i] = userNums[i];
				}
				userNums = bigger; // setting the old array equal to the temporary array
				bigger = null; // ensuring that only one array contains the user data
				}
			}
		}
		for(int i = 0; i < (userNums.length-1); i++) { // displaying values of array to user
			System.out.println(userNums[i]); // the -1 is because an extra zero is added to the array
			// as the array size increases due to the user entering "done"
		}
		
		System.out.println("Enter a file name: "); // prompting user for file name
		String userFileName = input.nextLine(); // storing user file name
		String FileName = "src/labs/lab3/" + userFileName + ".txt"; // formatting the file name
		// entered by the user
		System.out.println("File name is" + FileName); // displaying file name to user
		input.close(); // closing scanner
		
		try {
			FileWriter f = new FileWriter(FileName); // creating filewriter
			for (int i = 0; i < userNums.length-1; i++) { // copying array variables to new file
				f.write(userNums[i] + "\n");
		}
			f.flush(); // flushing the values
			f.close(); // closing the filewriter
		} catch (IOException e) { // catching errors
			System.out.println(e.getMessage()); // displaying error message to user
		}
		System.out.println("Make sure to refresh to see the file!"); // reminding user to refresh to
		// see the file also bc i forgot when i was testing
}
}

