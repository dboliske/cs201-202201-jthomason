// Jean-Yves Thomason; CS 201; Section One; 02/08/22; Exercise One; GradeAverage3
package labs.lab3;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

public class GradeAverage3 {

	public static void main(String[] args) throws IOException {
		File f = new File("src/labs/lab3/grades.csv"); // creating file object
		Scanner input = new Scanner(f); // creating scanner 
		double[] grades = new double[14]; // initializing array
		int count = 0; // initializing index variable
		while (input.hasNextLine()) { // reading in file values
			String line = input.nextLine();  // splitting each line
			String[] values = line.split(",");
			grades[count] = Double.parseDouble(values[1]); // converting values into double and storing
			// in array
			count++; // increasing count
		}
		double total = 0; // initializing sum variable
		for(int i = 0; i < grades.length; i++) {
			total = total + grades[i]; // find the total of each element in the array
	}
		double average = total / grades.length; // calculating average
		System.out.println("Average Grade is: " + average); // displaying average
		input.close(); // closing scanner
}
}