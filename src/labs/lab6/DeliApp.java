package labs.lab6;

import java.util.ArrayList;
import java.util.Scanner;

public class DeliApp {
	
	static public String nameOfCustomer(Scanner input) { // creating method to take in customer name to use in add customer
		System.out.println("Enter the name of the customer you would like to add: "); // prompting user for input
		String newCustomer = input.nextLine(); // storing user input to new string
		return newCustomer; // returns name of customer to be added 
	}
	
	static public String addCustomer(ArrayList<String> names, String newCustomer) { // creating method to add customer to array
		// list
		names.add(newCustomer); // calling add method from ArrayList class
		String result = newCustomer + "'s position in line: " + names.size(); // displaying the position of the customer
		// using names.size() since the customer that is added is always last in line
		return result; // returns string to indicate the position of the newly added customer
	}
	
	static public String helpCustomer(ArrayList<String> names) { // creating method to help customer
		if(!(names.size() > 0)) { // checks if the arraylist is empty or not
			return "There are no customers to be help"; // if the arraylist is empty there are no customers to help
		}
		
		else {
			String helpedCustomer = names.get(0); // saving name of customer being helped
			names.remove(0); // removing helped customer from array list
			return helpedCustomer + " has been helped"; // displaying name of helped customer
		}
	}
	
	
	
	

	public static void main(String[] args) { // creating menu in main method
		Scanner input = new Scanner(System.in); // creating scanner
		boolean done = false; // creating flag control variable
		System.out.println("Choose an option: "); // displaying options
		System.out.println("1. Add Customer to Queue ");
		System.out.println("2. Help Customer ");
		System.out.println("3. Exit ");
		String userChoice = input.nextLine(); // saving user input
		ArrayList<String> names = new ArrayList<String>(); // creating new ArrayList
		
		while(!done) { // using while loop to see whether or not user has chosen to exit
			switch(userChoice) {
			case "1": // calling the add customer method
				String newCustomer = nameOfCustomer(input);
				addCustomer(names,newCustomer);
				break;
			case "2": // calling help customer method
				helpCustomer(names);
				break;
			case "3": // changing flag variable's value to exit loop
				done = true;
				break;
			default: // default in case user enters invalid answer
				System.out.println("Please select a valid option");
			}
		}
		
	}

}
