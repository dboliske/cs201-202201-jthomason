// Jean-Yves Thomason; CS 201; Section One; 01/18/22; Exercise Five; SquareExercise

package labs.lab0;

public class SquareExercise {

	public static void main(String[] args) {
		// My goal is to create a five by five hollow square comprised of Xs
		// Step 1: Print out five Xs to the console and start a new line
		// Step 2: Print out an X followed by three spaces, add another X and start a new line
		// Step 3: Repeat step 2 twice, making sure to add a new line after each print command
		// Step 4: Repeat step 1
		
		System.out.println("XXXXX"); // Print the first row of Xs and start a new line
		System.out.println("X   X"); // Print Xs with spaces inbetween
		System.out.println("X   X");
		System.out.println("X   X");
		System.out.println("XXXXX"); // Finish the square by printing the last row of Xs
	}

}

// While I got the result I expected, the resulting output looks more like a rectangle than a square
// because there is more space between the Xs in the columns, than there is between the Xs in the rows.

