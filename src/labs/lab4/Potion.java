package labs.lab4;

public class Potion {
	private String name; // creating variables
	private double strength;
	
	public Potion() { // creating default constructor
		name = "invisability";
		strength = 5;
	}
	
	public Potion(String Name, double Strength) { // creating non-default constructor
		name = "invisability";
		setName(Name);
		strength = 5;
		setStrength(Strength);
	}
	
	public String getName() { // creating accessor methods
		return name;
	}
	
	public double getStrength() {
		return strength;
	}
	
	public void setName(String name) { // creating mutator methods
		this.name = name;
	}
	
	public void setStrength(double strength) { 
		this.strength = strength;
	}
	
	public String toString() { // creating to-string method
		return "Potion: " + name + ", Strength: " + strength;
	}
	
	public boolean validStrength() { // creating boolean method
		if(strength > 0 && strength < 10) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public boolean equals(Potion p) { // creating equals method
		if(this.name.equalsIgnoreCase(p.name) && this.strength == p.strength){
			return true;
		}
		else {
			return false;
		}
	}
}
