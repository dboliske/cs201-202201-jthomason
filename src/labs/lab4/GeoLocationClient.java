package labs.lab4;

public class GeoLocationClient {

	public static void main(String[] args) {
		GeoLocation g1 = new GeoLocation(); // testing default constructor
		
		System.out.println(g1.toString());  
		
		g1.setLat(20); // testing mutator methods
		
		g1.setLng(14);
		
		System.out.println(g1.toString());
		
		System.out.println(g1.getLat()); // testing accessor methods
		
		System.out.println(g1.getLng());
		
		GeoLocation g2 = new GeoLocation(80,30);  // testing non-default constructor
		
		System.out.println(g2.toString()); 
		
		System.out.println(g2.validLat()); // testing boolean methods (true case)
		
		System.out.println(g2.validLng());
		
		GeoLocation g3 = new GeoLocation(-200,200);
		
		System.out.println(g3.validLat()); // testing boolean methods (false case)
		
		System.out.println(g3.validLng());
		
		System.out.println(g1.equals(g2)); // testing equals method (false case)
		
		GeoLocation g4 = new GeoLocation(20,14);
		
		System.out.println(g1.equals(g4)); // (true case)
	}

}
