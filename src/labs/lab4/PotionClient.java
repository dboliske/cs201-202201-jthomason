package labs.lab4;

public class PotionClient {

	public static void main(String[] args) {
		Potion p1 = new Potion(); // testing default constructor
		
		System.out.println(p1);
		
		p1.setName("Super Strength"); // testing mutator methods
		
		p1.setStrength(9.9);
		
		System.out.println(p1);
		
		Potion p2 = new Potion("Flying",4); // testing non-default constructors
		
		System.out.println(p2);
		
		System.out.println(p2.getName()); // testing accessor methods
		
		System.out.println(p2.getStrength());
		
		Potion p3 = new Potion("Super Speed", 20);
		
		System.out.println(p3.validStrength()); // testing boolean methods (false case)
		
		System.out.println(p1.validStrength()); // (true case)
		
		Potion p4 = new Potion("Super Strength", 9.9);
		
		System.out.println(p4.equals(p3)); // testing equals method (false case)
		
		System.out.println(p4.equals(p1)); // (true case)

	}

}
