package labs.lab4;

public class PhoneNumberClient {

	public static void main(String[] args) {
		PhoneNumber pn1 = new PhoneNumber(); // testing default constructor
		
		System.out.println(pn1.toString());
		
		PhoneNumber pn2 = new PhoneNumber("60","243","1042592"); // testing non-default constructor
		
		System.out.println(pn2.toString());
		
		pn2.setCountryCode("10"); // testing mutator methods
		
		pn2.setAreaCode("323");
		
		pn2.setNumber ("2395402");
		
		System.out.println(pn2.toString()); 
		
		System.out.println(pn1.getCountryCode()); // testing accessor methods
		
		System.out.println(pn2.getAreaCode());
		
		System.out.println(pn2.getNumber());
		
		System.out.println(pn2.validAreaCode()); // testing boolean methods (true case)
		
		System.out.println(pn2.validNumber());
		
		PhoneNumber pn3 = new PhoneNumber("40","24","123123123123123");
		
		System.out.println(pn3.validAreaCode()); // testing boolean methods (false case)
		
		System.out.println(pn3.validNumber());
		
		System.out.println(pn2.equals(pn3)); // testing equals method (false case)
		
		PhoneNumber pn4 = new PhoneNumber("10","323","2395402");
		
		System.out.println(pn2.equals(pn4)); // testing equals method (true case)
		
	}

}
