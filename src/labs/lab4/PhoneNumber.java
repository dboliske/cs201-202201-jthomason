package labs.lab4;

public class PhoneNumber {
	private String countryCode; // creating variables 
	private String areaCode;
	private String number;
	
	public PhoneNumber() { // creating default constructor 
		countryCode = "1";
		areaCode = "713";
		number = "8554394";
	}
	
	public PhoneNumber(String cCode, String aCode, String Number ) { // creating non-default constructor
		countryCode = "1";
		setCountryCode(cCode);
		areaCode = "713";
		setAreaCode(aCode);
		number = "8554394";
		setNumber(Number);
	}
	
	public String getCountryCode() { // creating accessor methods
		return countryCode;
	}
	
	public String getAreaCode() {
		return areaCode;
	}
	
	public String getNumber() {
		return number;
	}
	
	public void setCountryCode(String countryCode) { // creating mutator methods
		this.countryCode = countryCode;
	}
	
	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}
	
	public void setNumber(String number) {
		this.number = number;
	}
	
	public String toString() { // creating to-string method
		return "+" + countryCode + " (" + areaCode + ")-" + number;
	}

	public boolean validAreaCode() { // creating boolean methods
		if(areaCode.length() == 3) {
			return true;
	}
		else {
			return false;
		}
}
	public boolean validNumber() {
		if(number.length() == 7) {
			return true;
	}
		else {
			return false;
		}
	}
	
	public boolean equals(PhoneNumber pn) { // creating equals method
		if( this.areaCode.equals(pn.getAreaCode()) && this.number.equals(pn.getNumber())) {
			return true;
		}
		else {
			return false;
		}
	}
}
