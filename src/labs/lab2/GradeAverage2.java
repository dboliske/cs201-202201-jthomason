// Jean-Yves Thomason; CS 201; Section One; 01/30/22; Exercise Two; GradeAverage2

package labs.lab2;

import java.util.Scanner;

public class GradeAverage2 {

	public static void main(String[] args) {
		// create scanner
		Scanner input = new Scanner(System.in);
		
		// initialize counter, sum and user input
		int userNum = -1;  // -1 to fix off-by-one bug
		double userSum = 0;
		double userGrade = 0;
		
		// do-while loop so the prompt appears at least one time 
		do {
			
			// prompt user for input
			System.out.println("Enter a grade (Enter -1 to Stop)");
			
			// store user input
			userGrade = Double.parseDouble(input.nextLine());
			
			// add new grade to ongoing sum
			userSum = userSum + userGrade;
			
			// increase counter by 1
			userNum = userNum + 1;
			
		} while(userGrade != -1); // condition in order to allow user to exit loop 
		
		double userAvg = (userSum+1) / userNum; // calculating average, one is added to the userSum 
		// in to account for the -1 required to exit loop
		
		System.out.println("The average grade is " + userAvg); // display average grade
		
		input.close(); // close scanner
	}
}

//TEST TABLE: 
// Example One:
	// | Input   | Expected Output|   Actual Output  |
	// |   15    | 36.1666666667  |36.166666666666664|
	// |   21    |                |                  |
	// |   50    |                |                  |
	// |   42    |                |                  |
	// |   66    |                |                  |
	// |   23    |                |                  |
//Example Two:
	// |  Input  | Expected Output|   Actual Output  |
	// |  78.2   | 89.7333333333  |89.73333333333333 |
	// |  88.9   |                |                  |
	// |  86.3   |                |                  |
	// |  90.1   |                |                  |
	// |  94.9   |                |                  |
	// |  100    |                |                  |
//Example Three:
	// |  Input  | Expected Output|   Actual Output  |
	// |  28.2   | 69.9333333333  | 69.93333333333332|
	// |  49.5   |                |                  |
	// |  70.7   |                |                  |
	// |  83.4   |                |                  |
	// |  91.1   |                |                  |
	// |  96.7   |                |                  |

// My code worked with high and low values, as well as decimal values very accurately (with the exception
// of some minor precision issues). I didn't test with negative values as it doesn't make sense in the 
// context of the problem.


