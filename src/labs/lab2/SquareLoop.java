// Jean-Yves Thomason; CS 201; Section One; 01/30/22; Exercise One; SquareLoop

package labs.lab2;

import java.util.Scanner;

public class SquareLoop {

	public static void main(String[] args) {
		// create scanner
		Scanner input = new Scanner(System.in);
		
		// prompt user for input
		System.out.println("What are the dimensions of your square?: ");
		
		// store user input and convert to integer
		int dim = Integer.parseInt(input.nextLine());
		
		// for loop for number of rows
		for (int row = 0; row < dim; row++ ) {
			for (int col = 0; col < dim; col++) { // for loop for number of columns
				
				System.out.print(" *"); // print character at each new column
			}
			// printing out new line at each new row
			System.out.println();
		}
	
		// close scanner
		input.close();

	}
}


