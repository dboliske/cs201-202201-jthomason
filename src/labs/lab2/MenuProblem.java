// Jean-Yves Thomason; CS 201; Section One; 01/30/22; Exercise Three; MenuProblem

package labs.lab2;

import java.util.Scanner;

public class MenuProblem {

	public static void main(String[] args) {
		
		// create scanner
		Scanner input = new Scanner(System.in);
		
		// initialize user input 
		String userChoice = "";
		
		// flagging control variable
		boolean done = false;
		
		while(!done) { // condition
			
			// print out menu options
			System.out.println("1: Print Hello World");
			System.out.println("2: Addition");
			System.out.println("3: Multiplication");
			System.out.println("4: Exit Program");
			System.out.println("Choose an Option: ");
			
			// store user input
			userChoice = input.nextLine();
			
			switch(userChoice) {
			case "1": 
				System.out.println("Hello World"); // print hello world
				break;
			case "2":
				System.out.println("Enter first number: "); // prompt user for first number
				double userNum1 = Double.parseDouble(input.nextLine()); // store first user number
				System.out.println("Enter second number: "); // prompt user for second number
				double userNum2 = Double.parseDouble(input.nextLine()); // store second user number
				double result = userNum1 + userNum2; // add numbers
				System.out.println(userNum1 + " + " + userNum2 + " is " + result); // display results
				break;
			case "3":
				System.out.println("Enter first number: "); // prompt user for first number
				double userNum3 = Double.parseDouble(input.nextLine()); // store first user number
				System.out.println("Enter second number: "); // prompt user for second number
				double userNum4 = Double.parseDouble(input.nextLine()); // store second user number
				double result2 = userNum3 * userNum4; // multiply numbers
				System.out.println(userNum3 + " + " + userNum4 + " is " + result2); // display results
				break;
			case "4":
				done = true; // changing flag control variable to exit program
				break;
			default: // default option in case invalid option is chosen
				System.out.println("Please enter a valid option: ");
			
			}
			
		}
		
		input.close(); // close scanner
	}

}
