package labs.lab5;

import java.io.File;
import java.util.Scanner;

public class CTAStopApp {
	
	public static CTAStation[] readFile(String filename) { // creating readFile method
		filename = "src/labs/lab5/CTAStops.csv"; // storing filename
		CTAStation[] stations = new CTAStation[34]; // creating CTAStation array
		int count = 0; // initializing count
		try {
			File f = new File(filename); // creating new file object
			Scanner input = new Scanner(f); // creating scanner
			while(input.hasNextLine()) { // reading file
				CTAStation s = null; // creating null CTAStation object
				String line = input.nextLine(); 
				String values[] = line.split(","); // splitting lines by comma and storing data in array
				String name = values[0]; // setting name equal to first value in line
				double t = Double.parseDouble(values[1]); // lat to second value
				double g = Double.parseDouble(values[2]); // lng to third value
				String location = values[3]; // location to fourth value
				String wc = values[4]; // wc to fifth value
				String op = values[5]; // open to sixth value
				boolean wheelchair; // initializing boolean variables
				boolean open;
				
				if(wc.equalsIgnoreCase("true")) { // changing boolean variables depending on the data in the file
					wheelchair = true;
				}else {
					wheelchair = false;
				}
				
				if(op.equalsIgnoreCase("true")) {
					open = true;
				} else {
					open = false;
				}
				
				s = new CTAStation(name, t, g, location, wheelchair, open); // creating new CTAStation object
				
				stations[count] = s; // storing in CTAStation array
				
				count++; // incrementing count
			}
			input.close(); // closing scanner
		} catch(Exception e) { // catching errors
			System.out.println(e.getMessage()); // printing error messages
		}
		return stations; // returning CTAStation array
	}
	
	public static void menu(Scanner input, CTAStation[] stations) { // creating menu method
		boolean done = false; // creating flagging variable
		do {
			System.out.println("1. Display Station Names"); // displaying menu options
			System.out.println("2. Display Stations with/without Wheelchair Access");
			System.out.println("3. Display Nearest Station");
			System.out.println("4. Exit");
			String userChoice = input.nextLine(); // storing user choice
			switch(userChoice) { // switch userChoice and each case calls a different method depending on the users answer
			case "1":
				displayStationNames(stations);
				break;
			case "2":
				displayByWheelchair(stations,input);
				break;
			case "3":
				displayNearest(stations,input);
				break;
			case "4":
				done = true; // changing flagging variable
				break;
			default:
				System.out.println("Please enter a valid input"); // in case user enters invalid input
			}

		}while(!done);
		
	}
	
	public static void displayStationNames(CTAStation[] stations) { // creating new method
		for(int i = 0; i < stations.length; i++) { // indexing through array
			System.out.println(stations[i].getName()); // displaying station name at index
		}
	}
	
	public static void displayByWheelchair(CTAStation[] stations, Scanner input) { // creating new method
		System.out.println("Please enter your wheelchair accessability preference (y/n): "); // prompting user for preference
		String userChoice = input.nextLine(); // storing user choice
		boolean userPref = false; // initializing user preference
		switch(userChoice.toLowerCase()) { // switching userChoice
		case "y": 
		case "yes":
			userPref = true; // changing userPref based on user input
			break;
		case "n":
		case "no":
			userPref = false;
			break;
		}
		boolean found = false; // flagging variable to display message if no station is found
		for(int i = 0; i < stations.length; i++) { // indexing through stations array
			if(stations[i].hasWheelchair() == userPref) { // checking if station matches user preference
				found = true; // changing value of flagging variable
				System.out.println(stations[i].toString()); // call to string method for station at index
			}
		}
		
		if(!found){ // if no station is found matching user preference message is displayed
			System.out.println("A station matching your preference could not be found");
		}
		
	}
	
	public static void displayNearest(CTAStation[] stations, Scanner input) { // creating new method
		double lat = 0; // initializing lat/lng
		double lng = 0; 
		System.out.println("Enter a latitude: "); // prompting user for latitude
		lat = Double.parseDouble(input.nextLine()); // storing user latitude
		System.out.println("Enter a longitutde: "); // prompting user for longitude
		lng = Double.parseDouble(input.nextLine()); // storing user longitude
		double nearest = stations[0].calcDistance(lat,lng); // initializing minimum distance to distance from first
		// station in array
		int index = 0; // initializing index
		for(int i = 0; i < stations.length; i++) { //indexing through array
			if(nearest > stations[i].calcDistance(lat,lng)) { // checking if station at index is closer than minimum
				nearest = stations[i].calcDistance(lat,lng); // if station is closer, that station becomes new minimum
				index = i; // storing index
			}
		}
		
		System.out.println(stations[index].toString()); // displaying to string for station at index
		
	}
	
	
		

	public static void main(String[] args) { // main method
		Scanner input = new Scanner(System.in); // creating scanner 
		CTAStation[] stations = readFile("filename"); //calling readfile
		menu(input, stations); // calling menu method
		input.close(); // closing scanner
	}

}
