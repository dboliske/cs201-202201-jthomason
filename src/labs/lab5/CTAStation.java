package labs.lab5;

public class CTAStation extends GeoLocation{
	private String name; // creating variables
	private String location;
	private boolean wheelchair;
	private boolean open;
	
	public CTAStation() { // creating default constructor using superclass constructor
		super();
		name = "train";
		location = "station";
		wheelchair = false;
		open = false;
	}
	
	public CTAStation(String name, double t, double g, String location, boolean wheelchair, boolean open) {
		super(t,g); // creating non-default constructor using superclass constructor
		this.name = name;
		this.location = location;
		this.open = open;
	}
	
	public String getName() { // creating accessor methods
		return name;
	}
	
	public String getLocation() {
		return location;
	}
	
	public boolean hasWheelchair() {
		return wheelchair;
	}
	
	public boolean isOpen() {
		return open;
	}
	
	public void setName(String name) { // creating mutator methods
		this.name = name;
	}
	
	public void setLocation(String location) {
		this.location = location;
	}
	
	public void setWheelchair(boolean wheelchair) {
		this.wheelchair = wheelchair;
	}
	
	public void setOpen(boolean open) { 
		this.open = open;
	}
	
	public String toString() { // creating to string method
		String wheelchairAns = "";
		String openAns = "";
		
		if(wheelchair) { // using if else statement to display different messages depending on whether or not 
			// the station has wheelchair access and whether or not it's open
			wheelchairAns = "Yes";
		}
		else {
			wheelchairAns = "No";
		}
		
		if(open) {
			openAns = "Yes";
		}
		else {
			openAns = "No";
		}
		
		
		return "Station Name: " + name + "; Station Location: " + location + "; Wheelchair Access: " + wheelchairAns + 
				"; Open: " + openAns + "; Coordinates: " + super.toString(); 
	}
	
	public boolean equals(CTAStation ts) { // creating equals method utilizing superclass equals method
		if(!super.equals(ts)) {
			return false;
		}
		if(this.name.equalsIgnoreCase(ts.getName()) && this.location.equalsIgnoreCase(ts.getLocation())
			&& this.wheelchair == ts.hasWheelchair() && this.open == ts.isOpen()) {
			return true;
		}
		else {
			return false;
		}
	}
}
