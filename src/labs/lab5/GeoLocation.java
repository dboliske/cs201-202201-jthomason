package labs.lab5;

public class GeoLocation {
	private double lat; // creating variables
	private double lng;
	
	
	public GeoLocation() { // creating default constructor
		lat = 0;
		lng = 0;
	}
	
	public GeoLocation(double t, double g) { // creating non-default constructor
		lat = 0;
		setLat(t);
		lng = 0;
		setLng(g);
	
	}
	public double getLat() { // creating accessor methods
		return lat;
	}
	
	public double getLng() {
		return lng;
	}
	
	public void setLat(double lat) { // creating mutator methods
		this.lat = lat;
	}
	
	public void setLng(double lng) {
		this.lng = lng;
	}
	
	public String toString() { // creating to-string method
		return "(" + lat + "," + lng + ")";
	}
	
	public boolean validLat() { // creating boolean methods
		if(lat < -90 || lat >90)
			return false;
		else
			return true;
	}
	
	public boolean validLng() {
		if(lng < -180 || lng > 180)
			return false;
		else
			return true;
	}
	
	public boolean equals(GeoLocation g) { // creating equals method
	if (this.lat != g.getLat())
		return false;
	else if (this.lng != g.getLng())
		return false;
	else
		return true;
}
	public double calcDistance(GeoLocation g) { // creating calcDistance method with GeoLocation object as input
		return Math.sqrt(Math.pow(this.lat - g.getLat(), 2) + Math.pow(this.lng - g.getLng(), 2));
	}
	
	public double calcDistance(double lat, double lng) { // creating calcDistance method with lat and lng as input
		return Math.sqrt(Math.pow(this.lat - lat, 2) + Math.pow(this.lng - lng, 2));
	}
}
