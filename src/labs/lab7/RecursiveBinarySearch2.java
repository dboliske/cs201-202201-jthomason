package labs.lab7;

import java.util.Scanner;

public class RecursiveBinarySearch2 {
	

	public static int search(String[] array, int start, int end, String value) { // creating search class
		if(start > end) { // checking condition because if the start is greater than the end than the
			// search term is not in the given array
			return -1;
		}
		
		int middle = start + (end - start) / 2; // altered middle formula to prevent overflow
		
		if(array[middle].equalsIgnoreCase(value)) { // comparing array at middle to search term
			return middle; // if the term is equal to the array at middle return the index of middle
		}
		
		else if(array[middle].compareTo(value) < 0) {
			return search(array,start,middle-1,value); // if the value is supposed to be to the left, recursively call the 
		} // search method
		
		else {
			return search(array,middle+1,end,value); // if the the value is not equal to, or not to the left then it must be to 
			// the right
		}
		
	}
	
	public static void main(String[] args) { // creating main method
		String[] array = {"c", "html", "java", "python", "ruby", "scala"}; // creating array
		Scanner input = new Scanner(System.in); // creating scanner
		System.out.println("Search for term: "); // prompting user for term
		int n = array.length; 
		String searchTerm = input.nextLine(); // saving user input
		int index = search(array,0,n-1,searchTerm); // calling search method
		if(index == -1) { // if search returns -1 then the item is not in the array
			System.out.println(searchTerm + " was not found");
		}
		else { // return index
			System.out.println(searchTerm + "was found at index " + index);
		}
		
		input.close(); // close scanner
	}
}
