package labs.lab7;

public class SelectionSort {

	public static void main(String[] args) { // creating main method
		double[] array = {3.142, 2.718, 1.414, 1.732, 1.202, 1.618, 0.577, 1.304, 2.685, 1.282}; // creating double array
		
		sort(array); // calling sort method
		
		for(int i = 0; i < array.length; i++) { // displaying values of sorted array
			System.out.print(array[i] + " ");
		}
		

	}
	
	public static double[] sort(double[] array) {
		for (int i=0; i<array.length - 1; i++) { // indexing through array
			int min = i; // initializing index of minimum vale
			for (int j=i+1; j<array.length; j++) { // comparing every other element of the array to initial minimum
				if (array[j] < array[min]) {
					min = j; // setting minimum to index of smaller value
				}
			}
			
			if (min != i) { // swapping values if new minimum is found
				double temp = array[i];
				array[i] = array[min];
				array[min] = temp;
			}
		}
	
	return array; // returning array

}
}
