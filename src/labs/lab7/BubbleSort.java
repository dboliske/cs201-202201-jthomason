package labs.lab7;

public class BubbleSort {

	public static void main(String[] args) { // creating main method
		int[] array = {10, 4, 7, 3, 8, 6, 1, 2, 5, 9}; // creating integer array
		
		sort(array); // calling sort method
		
		
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " "); // displaying elements of sorted array
		}
	

	}
	
	public static int[] sort(int[] array){ // creating sort method
		boolean done = false; // creating flag variable
		do {
			done = true; 
			for(int i = 0; i < array.length - 1; i++) { // indexing through elements of array
				// array.length - 1 used to prevent indexing outside of array length
				if(array[i+1] < array[i]) { // comparing two numbers next to each other
					int temp = array[i+1]; // if the number to the right is less than the number
					// to the left, the numbers are swapped
					array[i+1] = array[i];
					array[i] = temp;
					done = false; // changing boolean
				}
			}
			
		}while(!done);
		
		return array; // returning array
	}

}
