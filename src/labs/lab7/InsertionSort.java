package labs.lab7;

public class InsertionSort {

	
	public static String[] sort(String[] array) { // creating sort method
		for (int k=1; k<array.length; k++) { // starting with second element of array and indexing
			// through
			int i = k;
			while (i > 0 && array[i].compareTo(array[i-1]) < 0) { // comparing index at i
				// to everything to the left of i
				String temp = array[i]; // swapping values if the item to the left of i
				// is greater than i
				array[i] = array[i-1];
				array[i-1] = temp;
				i--; // decrementing i
			}
		}
		
		return array; // returning array
	}
	
	public static void main(String[] args) { // creating main method
		String[] array = {"cat", "fat", "dog", "apple", "bat", "egg"}; // creating array
		
		sort(array); // calling sort method
		
		for(int i = 0; i < array.length; i++) {
			System.out.print(array[i] + " "); // displaying elements of sorted array
		}

	}

}
