// Jean-Yves Thomason; CS 201; Section One; 01/24/22; Exercise Five; BoxWood

package labs.lab1;

import java.util.Scanner;

public class BoxWood {

	public static void main(String[] args) {
		
		// creating scanner
		Scanner input = new Scanner(System.in);
		
		// prompting user for length
		System.out.println("Enter the length in inches: ");
		
		// saving user length
		double length = Double.parseDouble(input.nextLine());
		
		// prompting user for width
		System.out.println("Enter the width in inches: ");
		
		// saving user width
		double width = Double.parseDouble(input.nextLine());
		
		// prompting user for depth
		System.out.println("Enter the depth in inches: ");
		
		// saving user depth
		double depth = Double.parseDouble(input.nextLine());
		
		// creating "conversion factor" so the division will result in a double
		double conFact = 144;
		
		// calculating the amount of wood needed and converting square inches to square feet by dividing by
		// 144.
		double woodAmSqFt = ((2 * depth * width) + (2 * depth * length) + (2 * length * width))/(conFact);
		
		// display how much wood is required for a 6-sided box with the specified dimensions
		System.out.println("For a box with dimensions " + width + " X " + length + " X "
				+ depth + " in inches, " + woodAmSqFt + " square feet of wood is required.");
		
		// close scanner
		input.close();

	}
	
	// TEST TABLE: 
	// Example 1: 
	// | Input Type |  Input  | Expected Output |    Actual Output   |
	// |   Length   |  0.86   |  0.00940833333  |0.009408333333333333|
	// |   Width    |  0.23   |                 |                    |
	// |   Depth    |  0.44   |                 |                    |
	// 
	// Example 2:
	// | Input Type |  Input  | Expected Output |    Actual Output   |
	// |   Length   |   15    |     194.375     |       194.375      |
	// |   Width    |   45    |                 |                    |
	// |   Depth    |   222   |                 |                    |
	//
	// Example 3: 
	// | Input Type |  Input  | Expected Output |    Actual Output   |
	// |   Length   |  231.4  |  3326.10611111  | 3326.106111111111  |
	// |   Width    |  538.1  |                 |                    |
	// |   Depth    |  149.4  |                 |                    |
	
	// I'm fairly happy with my program's accuracy, as it's able to accurately calculate the amount
	// of wood needed with enough accuracy to be practically effective. The only differences between my
	// expected outputs and actual outputs is the amount of repeating digits which is due to the website
	// I used to verify my answers displays less repeating digits than Java does.
	
}
