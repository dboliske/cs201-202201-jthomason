// Jean-Yves Thomason; CS 201; Section One; 01/24/22; Exercise Six; InchToCM

package labs.lab1;

import java.util.Scanner;

public class InchToCM {

	public static void main(String[] args) {
		
		// creating scanner
		Scanner input = new Scanner(System.in);
		
		// prompting user for length in inches
		System.out.println("Please enter a length in inches: ");
		
		// saving user input
		double userInch = Double.parseDouble(input.nextLine());
		
		// converting inches to cm
		double userCM = userInch * 2.54;
		
		// displaying conversion to user
		System.out.println(userInch + " inches is " + userCM + " centimeters.");
		
		// closing scanner
		input.close();
		
	}
	
	// TEST TABLE:
	// | Input | Expected Output |       Actual Output    |
	// | 10 IN |     25.4 CM     |          25.4 CM       |
	// | 63 IN |    160.02 CM    |         160.02 CM      |
	// | 257 IN|    652.78 CM    |         652.78 CM      |
	// |50.7 IN|   128.778 CM    | 128.77800000000002 CM  |
	// |66.98IN|  170.1292 CM    | 170.12920000000003 CM  |
	
	// I'm happy with this program as it's able to convert both decimal and integer values very accurately,
	// however when decimal values are entered, the output has a very small value added at the end. 
	// because this value is so small, for practical uses this added value can be ignored, so overall I 
	// think it's still an effective program despite the precision issue.
	
	
	
	
}
