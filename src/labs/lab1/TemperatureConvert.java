// Jean-Yves Thomason; CS 201; Section One; 01/24/22; Exercise Four; TemperatureConvert

package labs.lab1;

import java.util.Scanner;

public class TemperatureConvert {

	public static void main(String[] args) {
		
		// creating scanner
		 Scanner input = new Scanner(System.in);
		 
		// creating the "conversion factor" for the temperature conversion
		 double a = 5; 
		 double b = 9;
		 double conFact = a/b;
		 double conFactR = b/a;
		
		 // prompting user for temperature in fahrenheit
		 System.out.println("Enter a temperature in Fahrenheit: "); 
		
		 // saving user input 
		 double userTempF = Double.parseDouble(input.nextLine());
		
		 // converting temperature from fahrenheit to celsius
		 double convTempC = ((userTempF - 32)) * (conFact);
		 
		 // displaying the temperature in celsius
		 System.out.println(userTempF + " degrees fahrenheit is " + convTempC + " degrees celcius.");
		 
		 // prompting user for temperature in celsius
		 System.out.println("Enter a temperature in Celsius: ");
		 
		 // saving user input
		 double userTempC = Double.parseDouble(input.nextLine());
		 
		 // converting temperature from celsius to fahrenheit
		 double convTempF = ((userTempC * conFactR)) + 32;
		 
		 // displaying temperature in celsius
		 System.out.println(userTempC + " degrees celcius is " + convTempF + " degrees in fahrenheit.");
		 
		 // closing scanner
		 input.close();
		
	}
	
	// TEST TABLE: 
	// For Fahrenheit:
	// |      Input    |   Expected Output  |     Actual Output         |
	// |    -25 degF   | -31.6667 degC      | -31.666666666666668 degC  |
	// |  -10.89 degF  | -23.827778 degC    | -23.82777777777778 degC   |
	// |     0 degF    |  -17.7778 degC     | -17.77777777777778 degC   |
	// |     32 degF   |      0 degC        |          0.0 degC         |
	// |    43 degF    |   6.11111 degC     | 6.111111111111112 degC    |
	// |  78.44 degF   |    25.8 degC       |         25.8 degC         |
	
	// For Celsius:
	// |      Input    |   Expected Output  |     Actual Output         |
	// |    -35 degC   |    -31 degF        |       -31.0 degF          |
	// |  -13.23 degC  |    8.186 degF      |        8.186 degF         |
	// |     0 degC    |     32 degF        |        32.0 degF          |
	// |    16 degC    |     60.8 degF      |        60.8 degF          |
	// |  25.83 degC   |    78.494 degF     |       78.494 degF         |
	
	// I'm pleased with results of my program as it is able to convert positive and negative temperatures, 
	// with either integer or decimals values very accurately. It is also good to see that my program can
	// do this converting from Fahrenheit to Celsius, as well as for Celsius to Fahrenheit. The only 
	// differences between the expected outputs and the actual outputs are the amount of repeated values,
	// which I think is due to the fact that the website I was using to obtain the expected outputs, did
	// not include as many repeated digits as Java does, however even with this difference considered the
	// program is still fairly accurate.
	
}
