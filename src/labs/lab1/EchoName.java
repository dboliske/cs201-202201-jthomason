// Jean-Yves Thomason; CS 201; Section One; 01/22/22; Exercise One; EchoName

package labs.lab1;

import java.util.Scanner;

public class EchoName {

	public static void main(String[] args) {
		// create scanner
		Scanner input = new Scanner(System.in);
		
		// prompt user for name
		System.out.println("Enter a name:");
		
		// save user input
		String name = input.nextLine();
		
		// echo user input
		System.out.print(name);
		
		// close scanner
		input.close();

	}

}
