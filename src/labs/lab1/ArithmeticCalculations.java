// Jean-Yves Thomason; CS 201; Section One; 01/22/22; Exercise Two; ArithmeticCalculations

package labs.lab1;

public class ArithmeticCalculations {

	public static void main(String[] args) {
		
		int DadAge = 52 - 18;  // difference between my age and my father's age
		System.out.println(DadAge);
		
		int BirthYear = 2003 * 2; // my birth year multiplied by two
		System.out.println(BirthYear);
		
		double HeightInCM = 71 * 2.54; // converting my height from inches to centimeters
		System.out.println(HeightInCM);
		
		// converting feet in inches to feet and inches
		int feet = 71/12; // calculating number of feet
		int inches = 71%12; // calculating number of inches
		System.out.println(feet + " feet and " + inches + " inches.");
		
		
		
	
	}

}
