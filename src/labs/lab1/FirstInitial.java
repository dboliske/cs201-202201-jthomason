// Jean-Yves Thomason; CS 201; Section One; 01/22/22; Exercise Three; FirstInitial

package labs.lab1;

import java.util.Scanner;

public class FirstInitial {

	public static void main(String[] args) {
		
		// create scanner
		Scanner input = new Scanner(System.in);
		
		// prompt user for input
		System.out.println("Enter your first name: ");
		
		// save user input
		char i = input.nextLine().charAt(0);
		
		// display first initial
		System.out.println("Your first initial is " + i);
		
		// close scanner
		input.close();

	}

}
