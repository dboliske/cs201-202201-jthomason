package exams.first;

public class Pet {
	private String name;
	private int age;
	
	public Pet() {
		name = "Ewok";
		age = 2;
	}
	
	public Pet(String n, int a) {
		name = "Ewok";
		setName(n);
		age = 2;
		setAge(a);
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setAge(int age) {
		if(age > 0) {
			this.age = age;
		}
	}
	
	public String getName() {
		return name;
	}
	
	public int getAge() {
		return age;
	}
	
	public boolean equals(Pet p) {
		if(this.name.equalsIgnoreCase(p.getName()) && this.age == p.getAge()) {
			return true;
		}
		else {
			return false;
		}
	}
	
	public String toString() {
		return "Name: " + name + "; Age: " + age;
	}
}
