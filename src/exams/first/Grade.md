# Midterm Exam

## Total

96/100

## Break Down

1. Data Types:                  20/20
    - Compiles:                 5/5
    - Input:                    5/5
    - Data Types:               5/5
    - Results:                  5/5
2. Selection:                   20/20
    - Compiles:                 5/5
    - Selection Structure:      10/10
    - Results:                  5/5
3. Repetition:                  20/20
    - Compiles:                 5/5
    - Repetition Structure:     5/5
    - Returns:                  5/5
    - Formatting:               5/5
4. Arrays:                      17/20
    - Compiles:                 3/5
    - Array:                    4/5
    - Exit:                     5/5
    - Results:                  5/5
5. Objects:                     19/20
    - Variables:                5/5
    - Constructors:             5/5
    - Accessors and Mutators:   5/5
    - toString and equals:      4/5

## Comments

1. Good
2. Good
3. Good
4. Compiler issues related to assigning an empty array into the full one.
5. Nearly perfect, but the `equals` method does not follow the UML diagram and take an Object as a parameter.
