package exams.second;

import java.util.Scanner;

public class examQ5 {
	
	public static int jumpSearchCalc(double[] numbers, double value) {
		int step = (int)Math.sqrt(numbers.length);
		int lastIndex = step - 1;
		return jumpSearch(numbers,value,step,lastIndex);
	}
		
		public static int jumpSearch(double[] numbers, double value, int step, int lastIndex) {
			if(lastIndex < numbers.length && value > numbers.length) {
				lastIndex += step;
				return jumpSearch(numbers,value,step,lastIndex); 
			} else {
				for(int index = lastIndex - step + 1; index <= lastIndex && index < numbers.length;
						index++) {
					if(value == numbers[index]) {
						return index;
					}
				}
			}
			return -1;
		}
	
		
	
	public static void main(String[] args) {
		double[] numbers = {0.577, 1.202, 1.282, 1.304, 1.414, 1.618, 1.732, 2.685, 2.718, 3.142};
		double userNum = 0;
		int index = 0;
		Scanner input = new Scanner(System.in);
		System.out.println("Enter search term: ");
		userNum = Double.parseDouble(input.nextLine());
		index = jumpSearchCalc(numbers,userNum);
		if(index == -1) {
			System.out.println(userNum +  " was not found");
		} else {
			System.out.println(userNum + " was found at index " + index);
		}
		
		
		input.close();

	}

}
