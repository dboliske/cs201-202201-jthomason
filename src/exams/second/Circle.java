package exams.second;

public class Circle extends Polygon {
	private double radius;
	
	public Circle() {
		super();
		radius = 1;
	}
	
	public void setRadius(double radius) {
		if(radius > 1) {
			this.radius = radius;
		}
	}
	
	public double getRadius() {
		return radius;
	}
	
	public String toString() {
		return super.toString() + ", Radius: " + radius;
	}
	
	public double area() {
		return Math.PI * radius * radius;
	}
	
	public double perimeter() {
		return 2.0 * Math.PI * radius;
	}
	
	
}
