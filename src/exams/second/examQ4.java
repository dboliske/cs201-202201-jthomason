package exams.second;

public class examQ4 {
	
	public static String[] selectionSort(String[] words) {
		for (int i = 0; i < words.length-1; i++) {
			int min = i;
			for(int k = i+1; k < words.length; k++) {
				if(words[k].compareTo(words[min]) < 0) {
					min = k;
				}
			}
			if(min != 1) {
				String temp = words[i];
				words[i] = words[min];
				words[min] = temp;
			}
		}
		return words;
	}

	public static void main(String[] args) {
		String[] userWords = {"speaker", "poem", "passenger", "tale",
				"reflection", "leader", "quality", "percentage", "height",
				"wealth", "resource", "lake", "importance"};
		userWords = selectionSort(userWords);
		for(int i = 0; i < userWords.length; i++) {
			System.out.println(userWords[i]);
		}

	}

}
