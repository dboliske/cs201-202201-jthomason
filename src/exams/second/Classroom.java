package exams.second;

public class Classroom {
	private String building;
	private String roomNumber;
	private int seats;
	
	public Classroom() {
		building = "Stuart";
		roomNumber = "113";
		seats = 100;
	}
	
	public void setBuilding(String building) {
		this.building = building;
	}
	
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}
	
	public void setSeats(int seats) {
		if(seats >= 0) {
			this.seats = seats;
		}
	}
	
	public String getBuiliding() {
		return building;
	}
	
	public String getRoomNumber() {
		return roomNumber;
	}
	
	public int getSeats() {
		return seats;
	}
	
	public String toString() {
		return "Building Name: " + building + ", Room Number: " + roomNumber + ", Number of Seats: " + seats;
	}
	
	
}
