package exams.second;

import java.util.Scanner;
import java.util.ArrayList;

public class examQ3 {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		boolean done = false;
		String userResponse = null;
		ArrayList<Double> userNums = new ArrayList<Double>();
		do {
			System.out.println("Enter a number (Enter done to stop): ");
			userResponse = input.nextLine();
			if(userResponse.equals("done")){
				done = true;
			} else {
				userNums.add(Double.parseDouble(userResponse));
			}
			
		} while(!done);
		
		double userMax = userNums.get(0);
		for(Double d : userNums) {
			if(d > userMax) {
				userMax = d;
			}
		}
		
		double userMin = userNums.get(0);
		for(Double d : userNums) {
			if(d < userMin) {
				userMin = d;
			}
		}
		
		System.out.println("Maximum is " + userMax + ", Minimum is " + userMin);
		
		input.close();
		
	}

}
