package exams.second;

public abstract class Polygon {
	private String name;
	
	public Polygon() {
		name = "Hexagon";
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public String toString() {
		return "Polygon: " + name;
	}
	
	public abstract double area();
	
	public abstract double perimeter();
}
